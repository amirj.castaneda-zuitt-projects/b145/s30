// Synchronous - means perform one task at a time
// Asynchronous - we can proceed to execute other statements while time
// consuming code is running in the background. 

// fetch - asynchronously requests data
console.log(fetch('https://jsonplaceholder.typicode.com/posts'));
fetch('https://jsonplaceholder.typicode.com/posts').then(response => console.log(response.status))

async function fetchData() {

	let result = await fetch('https://jsonplaceholder.typicode.com/posts');
	console.log(result);

	let json = await result.json();
	console.log(json);
}

fetchData();

// Create a post
fetch('https://jsonplaceholder.typicode.com/posts', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'New Post',
		body: 'I am a new post',
		userId: 1
	})
}).then((response) => response.json()).then((json) => console.log(json));

// Update a post
fetch('https://jsonplaceholder.typicode.com/posts', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		id: 1,
		title: 'Updated Post',
		body: 'This is an updated post',
		userId: 1
	})
}).then((response) => response.json()).then((json) => console.log(json));

// Upadting post using PATCH method
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Corrected Post',
	})
}).then((response) => response.json()).then((json) => console.log(json));

// DELETE POST
fetch('https://jsonplaceholder.typicode.com/posts/12', {
	method: 'DELETE',
});

// Filtering Posts
fetch('https://jsonplaceholder.typicode.com/posts?userId=1').then((response) => response.json()).then((json) => console.log(json));

// Retrieving comments
fetch('https://jsonplaceholder.typicode.com/posts/1/comments')
.then((response) => response.json()).then((json) => console.log(json));