// fetch('https://jsonplaceholder.typicode.com/todos').then(response => response.json()).then(data => console.log(data));
fetch('https://jsonplaceholder.typicode.com/todos').then(response => response.json()).then(data => { let arr = []; data.map(item=> arr.push(item['title'])); console.log(arr)});
fetch('https://jsonplaceholder.typicode.com/todos/1').then(response => response.json()).then(data => {console.log(data); console.log(data['title'], data['completed'])});

fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'New Post',
		completed: false,
		userId: 1,
	})
}).then((response) => response.json()).then((json) => console.log(json));


fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-Type' : 'application/json'
	},
	body: JSON.stringify({
    userId: 1,
    id: 3,
    title: "fugiat veniam minus",
    completed: false
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-Type' : 'application/json'
	},
	body: JSON.stringify({
    userId: 1,
    description: 'hi',
    'Data Completed': '1/2/2018',
    title: "fugiat veniam minus",
    status: false
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers: {
		'Content-Type' : 'application/json'
	},
	body: JSON.stringify({
		completed: true
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers: {
		'Content-Type' : 'application/json'
	},
	body: JSON.stringify({
		completed: true,
		'Date Completed': '1/2/09'
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'DELETE',
});